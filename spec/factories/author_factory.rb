# frozen_string_literal: true

FactoryBot.define do
  factory :author do
    first_name { 'first' }
    last_name { 'last' }
    sequence :email do |n|
      "email#{n}@gmail.com"
    end
    date_of_birth { Date.today }
  end
end
