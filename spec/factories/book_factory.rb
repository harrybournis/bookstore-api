# frozen_string_literal: true

FactoryBot.define do
  factory :book do
    sequence :title do |n|
      "title #{n}"
    end
    description { 'description' }
    isbn { '9780062316110' }
    is_visible { false }
    date_of_creation { Date.today }
    sequence :order_position
    association :author, factory: :author
    association :publisher, factory: :publisher
  end
end
