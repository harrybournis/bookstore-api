# frozen_string_literal: true

FactoryBot.define do
  factory :publisher do
    sequence(:name) do |n|
      "publisher name #{n}"
    end
    telephone { '3393939393' }
    address { 'address' }
  end
end
