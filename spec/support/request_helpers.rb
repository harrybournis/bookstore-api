module RequestHelpers
  def json
    JSON.parse(response.body)
  end

  def data
    JSON.parse(response.body)['data']
  end
end
