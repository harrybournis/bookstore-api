require 'rails_helper'

RSpec.describe Author, type: :model do
  it { should have_many :books }
  it { should have_many(:publishers).through(:books) }
  it { should validate_presence_of :first_name }
  it { should validate_presence_of :last_name }
  it { should validate_presence_of :email }
  it { should validate_uniqueness_of :email }

  it 'validates email format' do
    author = Author.new email: 'email@email.com'
    author.validate
    expect(author.errors[:email]).to be_empty
  end

  it 'validates error email format' do
    author = Author.new email: 'emailemail.com'
    author.validate
    expect(author.errors[:email]).to_not be_empty
  end

  it 'full_name returns first_name and last_name' do
    author = Author.new first_name: 'first', last_name: 'last'
    expect(author.full_name).to eq 'first last'
  end
end
