require 'rails_helper'
require 'timecop'

RSpec.describe Book, type: :model do
  it { should belong_to(:publisher) }
  it { should belong_to(:author).dependent(:destroy) }
  it { should validate_presence_of :title }
  it { should validate_presence_of :description }
  it { should validate_presence_of :author }
  it { should validate_presence_of :isbn }

  it 'validates valid isbn' do
    valid_isbn_numbers = ['9780062316110', '978-0062316110', '159477336X', '978-1594773365']

    valid_isbn_numbers.each do |isbn|
      book = Book.new
      book.isbn = isbn
      book.validate
      expect(book.errors[:isbn]).to be_empty
    end
  end

  it 'returns error for invalid isbn' do
    invalid_isbn_numbers = ['9788363396110', '978-2062316540']

    invalid_isbn_numbers.each do |isbn|
      book = Book.new
      book.isbn = isbn
      book.validate
      expect(book.errors[:isbn]).to be_truthy
    end
  end

  it 'sets the date of creation when saved if nil' do
    date = Date.today
    Timecop.freeze(date) do
      book = build(:book, date_of_creation: nil)
      book.save
      expect(book.date_of_creation).to eq(date)
    end
  end

  it 'does not change the date_of_creation when saving if not nil ' do
    date_today = Date.today
    date_of_creation = Date.today - 2.days
    Timecop.freeze(date_today) do
      book = build(:book, date_of_creation: date_of_creation)
      book.save
      expect(book.date_of_creation).to eq(date_of_creation)
    end
  end

  it 'adds error if publisher_id does not exist' do
    invalid_publisher_id = 2291
    book = build :book, publisher_id: invalid_publisher_id

    expect(Publisher.exists?(invalid_publisher_id)).to be_falsy
    book.save
    expect(book.valid?).to be_falsy
    expect(book.errors[:publisher_id]).to be_truthy
  end

  it 'saves publisher_id if it exists' do
    publisher = create :publisher
    book = build :book, publisher_id: publisher.id

    expect(Publisher.exists?(publisher.id)).to be_truthy
    book.save
    expect(book.publisher_id).to eq publisher.id
  end

  it '.visible returns the visible books' do
    create :book, is_visible: false
    create :book, is_visible: true

    expect(Book.all.count).to eq 2

    expect(Book.visible.count).to eq 1
  end

  it '.with_publisher returns the books that have a publisher' do
    book1 = create :book
    book2 = create :book, publisher: nil

    expect(book1.publisher).to be_truthy
    expect(book2.publisher).to be_falsy

    expect(Book.all.count).to eq 2
    expect(Book.with_publisher.count).to eq 1
  end

  it '.ordered_alphabetically_by_author returns books sorted alphabetically by author' do
    book3 = create :book, author: create(:author, last_name: 'Clastname'), publisher: nil
    book1 = create :book, author: create(:author, last_name: 'Alastname'), publisher: nil
    book2 = create :book, author: create(:author, last_name: 'Blastname'), publisher: nil

    books = Book.ordered_alphabetically_by_author

    expect(books[0]).to eq book1
    expect(books[1]).to eq book2
    expect(books[2]).to eq book3
  end

  it '.ordered_by_order_position returns book ordered by their order_position' do
    book3 = create :book, order_position: 3, publisher: nil
    book1 = create :book, order_position: 1, publisher: nil
    book2 = create :book, order_position: 2, publisher: nil

    books = Book.ordered_by_order_position

    expect(books[0]).to eq book1
    expect(books[1]).to eq book2
    expect(books[2]).to eq book3
  end
end
