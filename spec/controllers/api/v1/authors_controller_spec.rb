# frozer_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::AuthorsController, type: :request do
  describe 'POST /authors' do
    it 'creates a new author' do
      author_params = attributes_for :author, first_name: 'first name'

      expect {
        post api_v1_authors_url, params: author_params
      }.to change { Author.count }.by 1

      assert_response :created

      author = JSON.parse(response.body)['author']
      expect(author['first_name']).to eq 'first name'
    end

    it 'returns 400 if author validation fails' do
      author_params = attributes_for :author, first_name: nil, email: 'wrong.com'

      expect {
        post api_v1_authors_url, params: author_params
      }.to_not change { Book.count }

      assert_response :bad_request

      errors = JSON.parse(response.body)['errors']
      expect(errors['first_name']).to be_truthy
    end
  end
end
