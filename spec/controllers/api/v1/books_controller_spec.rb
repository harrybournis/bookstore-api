# frozer_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::BooksController, type: :request do
  describe 'GET /books' do
    it 'returns active, with publisher books, ordered by author last name and order_position' do
      author1 = create(:author, last_name: 'Alastname')
      author2 = create(:author, last_name: 'Blastname')
      author3 = create(:author, last_name: 'Clastname')

      book3 = create :book, author: author2, is_visible: true, order_position: 2
      book2 = create :book, author: author2, is_visible: true, order_position: 1
      book4 = create :book, author: author3, is_visible: true, order_position: 1
      book6 = create :book, author: author3, is_visible: true, order_position: 3
      book5 = create :book, author: author3, is_visible: true, order_position: 2
      book1 = create :book, author: author1, is_visible: true
      book_no_publisher = create :book, author: author2, publisher: nil, is_visible: true
      book_not_visible = create :book, author: create(:author, last_name: 'Dlastname'), is_visible: false

      get api_v1_books_url

      assert_response :success
      data = JSON.parse(response.body)['books']

      # validate length
      expect(data.length).to eq 6

      # validate order
      expect(data[0]['title']).to eq book1.title
      expect(data[1]['title']).to eq book2.title
      expect(data[2]['title']).to eq book3.title
      expect(data[3]['title']).to eq book4.title
      expect(data[4]['title']).to eq book5.title
      expect(data[5]['title']).to eq book6.title

      # validate attributes presence
      json_book = data[0]
      expect(json_book['title']).to be_truthy
      expect(json_book['description']).to be_truthy
      expect(json_book['isbn']).to be_truthy
      expect(json_book['author']['full_name']).to be_truthy
    end

    it 'returns 204 and empty response if no books found' do
      get api_v1_books_url

      assert_response :no_content
    end

    it 'truncates description if more that 100 chars' do
      author1 = create(:author, last_name: 'Alastname')

      book1 = create :book, author: author1, is_visible: true, description: 'a' * 150

      get api_v1_books_url

      assert_response :success
      data = JSON.parse(response.body)['books']

      expect(data[0]['description'].length).to eq 103
      expect(data[0]['description'][-3..-1]).to eq '...'
    end
  end

  describe 'GET /books/:id' do
    let(:book) { create(:book, date_of_creation: Date.today) }

    it 'returns one book at which contains author and publisher' do
      get api_v1_book_path(book.id)

      assert_response :ok

      json = JSON.parse(response.body)['book']

      expect(json['id']).to eq book.id
      expect(json['author']['full_name']).to eq book.author.full_name
      expect(json['publisher']['name']).to eq book.publisher.name
    end

    it 'returns 204 no content if record not found' do
      get api_v1_book_path(236427848543)

      assert_response :no_content
    end
  end

  describe 'POST /books' do
    it 'creates a new book' do
      author = create :author
      book_params = attributes_for :book, author_id: author.id

      expect {
        post api_v1_books_url, params: book_params
      }.to change { Book.count }.by 1

      assert_response :created

      book = JSON.parse(response.body)['book']
      expect(book['title']).to eq book_params[:title]
    end

    it 'returns 400 if book validation fails' do
      book_params = attributes_for :book, title: nil

      expect {
        post api_v1_books_url, params: book_params
      }.to_not change { Book.count }

      assert_response :bad_request

      errors = JSON.parse(response.body)['errors']
      expect(errors['title']).to be_truthy
    end
  end

  describe 'PATCH /books/:id' do
    let(:book) { create :book, title: 'original' }

    it 'updates a book ' do
      book_params = { title: 'new', id: book.id }

      expect {
        patch api_v1_book_path(book.id), params: book_params
      }.to change { Book.find(book.id).title }.from('original').to('new')

      assert_response :ok

      book = JSON.parse(response.body)['book']
      expect(book['title']).to eq 'new'
    end

    it 'returns 400 if validation error' do
      book_params = { description: nil }

      expect {
        patch api_v1_book_path(book.id), params: book_params
      }.to_not change { Book.find(book.id).description }

      assert_response :bad_request

      errors = JSON.parse(response.body)['errors']
      expect(errors['description']).to be_truthy
    end

    it 'returns 400 if book not found' do
      book_params = { title: 'new' }

      patch api_v1_book_path(384975987492), params: book_params

      assert_response :bad_request

      errors = JSON.parse(response.body)['errors']
      expect(errors['id']).to be_truthy
    end
  end

  describe 'DELETE /books/:id' do
    it 'deletes a book at DELETE /books/:id' do
      book = create :book

      expect {
        delete api_v1_book_path(book.id)
      }.to change { Book.count }.from(1).to(0)

      assert_response :no_content
    end

    it 'returns 400 if book not found' do
      delete api_v1_book_path(384975987492)

      assert_response :bad_request

      errors = JSON.parse(response.body)['errors']
      expect(errors['id']).to be_truthy
    end
  end
end
