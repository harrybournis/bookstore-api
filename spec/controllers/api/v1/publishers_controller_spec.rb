# frozer_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::PublishersController, type: :request do
  describe 'POST /publishers' do
    it 'creates a new publisher' do
      publisher_params = attributes_for :publisher, name: 'name'

      expect {
        post api_v1_publishers_url, params: publisher_params
      }.to change { Publisher.count }.by 1

      assert_response :created

      publisher = JSON.parse(response.body)['publisher']
      expect(publisher['name']).to eq 'name'
    end

    it 'returns 400 if publisher validation fails' do
      publisher_params = attributes_for :publisher, name: nil

      expect {
        post api_v1_publishers_url, params: publisher_params
      }.to_not change { Book.count }

      assert_response :bad_request

      errors = JSON.parse(response.body)['errors']
      expect(errors['name']).to be_truthy
    end
  end
end
