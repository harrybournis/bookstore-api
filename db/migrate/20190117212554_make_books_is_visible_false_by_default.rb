class MakeBooksIsVisibleFalseByDefault < ActiveRecord::Migration[5.2]
  def change
    change_column :books, :is_visible, :boolean, default: false, null: false
  end
end
