class AddOrderPositionToBooks < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :order_position, :integer
  end
end
