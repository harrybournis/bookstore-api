class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :title
      t.text :description
      t.boolean :is_visible
      t.date :date_of_creation
      t.string :isbn
      t.bigint :author_id
      t.bigint :publisher_id

      t.timestamps
    end
    add_index :books, :author_id
    add_index :books, :publisher_id
  end
end
