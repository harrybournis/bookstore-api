# Bookstore API

This is a simple bookstore API created in Ruby on Rails with PostgreSQL database.

## Installation
1. Install Ruby version 2.6.0
2. Install PostgreSQL (tested with version 9.4)
3. Run `bundle install` in the app directory to install gems
4. Run `rake db:setup` in the app directory to create database and populate with sample data
5. For fresh data, running `rake db:schema:load` will recreate the database and `rake db:seed` will re-populate it
6. Run `rails s` in the app directory to start the server. App is running at `localhost:3000`

## Testing
Run `rspec` in the application directory to run the tests. In case this fails,
you may need to install the RSpec gem globally with the command `gem install rspec`.
To generate coverage with simplecov, run `COVERAGE=true rspec`. The results can be
seen in the file `<app-root>/coverage/index.html`.

## Documentation
Documentation is written using `Swagger`. The specification file can be found
at `<app-root>/public/swagger.yaml`, and the swagger ui export can be launched
with the `<app-root>/public/index.html` file. Naturally, it is also served by
Rails at `localhost:3000`.

## Pending
- Caching (Redis)
- Docker
