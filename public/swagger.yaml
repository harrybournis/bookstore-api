swagger: "2.0"
info:
  description: "Documentation for simple bookstore API, with Book, Author and Publisher."
  version: "1.0.0"
  title: "Bookstore"
  termsOfService: "http://swagger.io/terms/"
  contact:
    email: "harrybournis@gmail.com"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
basePath: "/api/v1"
consumes:
  - application/json
produces:
  - application/json
tags:
- name: "book"
  description: "A book is written by an Author, and published by a Publisher"
- name: "author"
  description: "Writes books"
- name: "publisher"
  description: "Publishes books"
schemes:
- "http"
- "https"
paths:
  /books:
    get:
      tags:
      - "book"
      summary: "Get Books"
      description: "Get all Books that are visible and have a publisher, ordered alphabetically by the Author's last name and order_position"
      operationId: "Get Books"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      responses:
        200:
          description: Books
          schema:
            type: object
            properties:
              books:
                type: array
                items:
                  $ref: "#/definitions/BookForCollection"
        204:
          description: No Books found

    post:
      tags:
      - "book"
      summary: "Create a new book"
      description: "Creates a new book"
      operationId: "Create Book"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Title, description, isbn and author_id are required. If not supplied, is_visible will default to false and date_of_creation to the current date."
        required: true
        schema:
          type: object
          required:
            - title
            - description
            - isbn
            - author_id
          properties:
            title:
              type: "string"
              example: "The Great Gatsby"
            description:
              type: "string"
              example: "The Great Gatsby is a 1925 novel written by American author F. Scott Fitzgerald that follows a cast of characters living in the fictional towns of West Egg and East Egg on prosperous Long Island in the summer of 1922. The story primarily concerns the young and mysterious millionaire Jay Gatsby and his quixotic passion and obsession with the beautiful former debutante Daisy Buchanan. Considered to be Fitzgerald's magnum opus, The Great Gatsby explores themes of decadence, idealism, resistance to change, social upheaval, and excess, creating a portrait of the Roaring Twenties that has been described as a cautionary tale regarding the American Dream."
            isbn:
              type: "string"
              description: "The unique number that identifies a book. Can be either in the 10-character or in the 13-character format."
              example: "9781411469570"
            author_id:
              type: "integer"
              format: "int64"
              example: 1
              description: "The id of the Author"
            publisher_id:
              type: "integer"
              example: 1
              format: "int64"
              description: "The id of the Publisher"
            date_of_creation:
              type: "string"
              description: "The date that the book was added to the system"
              example: "2019-01-21"
            is_visible:
              type: "boolean"
              description: "Whether the book is visible to the public. In false, it will not be included in GET /books"
              example: true
            order_position:
              type: integer
              description: "Determines the order that the Books from the same Author will appear in GET /books"
              example: 1
      responses:
        201:
          description: The created Book
          schema:
            $ref: "#/definitions/Book"
        400:
          description: Invalid parameters
          schema:
            type: object
            properties:
              errors:
                type: object
                properties:
                  author:
                    type: array
                    items:
                      type: "string"
                      example:
                        - "must exist"
                        - "can't be blank"
                  title:
                    type: array
                    items:
                      type: "string"
                      example:
                        - "can't be blank"
  /books/{id}:
    get:
      tags:
      - "book"
      summary: "Get Single Book"
      description: "Get Book that matches with the provided id"
      operationId: "Get Single Book"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
        - in: path
          name: id
          type: integer
          description: The id of the Book to be returned
          required: true
      responses:
        200:
          description: Books
          schema:
            $ref: "#/definitions/SingleBook"
        204:
          description: Book with provided id was not found
    patch:
      tags:
      - "book"
      summary: "Updates a book"
      description: "Updates a book"
      operationId: "Update Book"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
        - in: path
          name: id
          type: integer
          description: The id of the book to be updated
          required: true
        - in: "body"
          name: "body"
          required: false
          description: The book parameters to change
          schema:
            type: object
            required:
              - title
              - description
              - isbn
              - author_id
            properties:
              title:
                type: "string"
                example: "The Great Gatsby 2"
              is_visible:
                type: "boolean"
                description: "Whether the book is visible to the public. In false, it will not be included in GET /books"
                example: false
      responses:
        200:
          description: The updated book
          schema:
            $ref: "#/definitions/Book"
        400:
          description: Invalid parameters or book with the provided id not found
          schema:
            type: object
            properties:
              errors:
                type: object
                properties:
                  author:
                    type: array
                    items:
                      type: "string"
                      example:
                        - "must exist"
                        - "can't be blank"
                  title:
                    type: array
                    items:
                      type: "string"
                      example:
                        - "can't be blank"
    delete:
      tags:
      - "book"
      summary: "Deletes a book"
      description: "Deletes a book"
      operationId: "Delete Book"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
        - in: path
          name: id
          type: integer
          description: The id of the book to be deleted
          required: true
      responses:
        204:
          description: Success. The book was deleted.
        400:
          description: Book with the provided id not found
          schema:
            type: object
            properties:
              errors:
                type: object
                properties:
                  id:
                    type: array
                    items:
                      type: "string"
                      example:
                        - "does not exist"
  /authors:
    post:
      tags:
      - "author"
      summary: "Create a new Author"
      description: "Creates a new Author"
      operationId: "Create Author"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "First name, last name and email must all be present, and the email needs to be a valid email address."
        required: true
        schema:
          type: object
          required:
            - first_name
            - last_name
            - email
          properties:
            first_name:
              type: "string"
              example: "Scott"
            last_name:
              type: "string"
              example: "Fitzgerald"
            email:
              type: "string"
              example: scott@fitzgerald.com
            date_of_birth:
              type: string
              example: "2019-01-21"
      responses:
        201:
          description: The created Author
          schema:
            type: object
            properties:
              id:
                type: integer
                example: 1
              first_name:
                type: "string"
                example: "Scott"
              last_name:
                type: "string"
                example: "Fitzgerald"
              email:
                type: "string"
                example: scott@fitzgerald.com
              date_of_birth:
                type: string
                example: "2019-01-21"
        400:
          description: Invalid parameters
          schema:
            type: object
            properties:
              errors:
                type: object
                properties:
                  last_name:
                    type: array
                    items:
                      type: "string"
                      example:
                        - "can't be blank"
                  email:
                    type: array
                    items:
                      type: "string"
                      example:
                        - "is invalid"
  /publishers:
    post:
      tags:
      - "publisher"
      summary: "Create a new Publisher"
      description: "Creates a new Publisher"
      operationId: "Create Publisher"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Name, telephone and address must all be present. Name must be unique."
        required: true
        schema:
          type: object
          required:
            - name
            - telephone
            - address
          properties:
            name:
              type: "string"
              example: "Penguin Publishing Group"
            address:
              type: "string"
              example: "Address Str. 24"
            telephone:
              type: "string"
              example: "21987389129"
      responses:
        201:
          description: The created Publisher
          schema:
            type: object
            properties:
              id:
                type: integer
                example: 1
              name:
                type: "string"
                example: "Penguin Publishing Group"
              address:
                type: "string"
                example: "Address Str. 24"
              telephone:
                type: "string"
                example: "21987389129"
        400:
          description: Invalid parameters
          schema:
            type: object
            properties:
              errors:
                type: object
                properties:
                  name:
                    type: array
                    items:
                      type: "string"
                      example:
                        - "has already been taken"
                  address:
                    type: array
                    items:
                      type: "string"
                      example:
                        - "can't be blank"
definitions:
  Book:
    type: "object"
    required:
    - "title"
    - "description"
    - "isbn"
    - "author_id"
    properties:
      id:
        type: "integer"
        format: "int64"
        example: 1
      title:
        type: "string"
        example: "The Great Gatsby"
      description:
        type: "string"
        example: "The Great Gatsby is a 1925 novel written by American author F. Scott Fitzgerald that follows a cast of characters living in the fictional towns of West Egg and East Egg on prosperous Long Island in the summer of 1922. The story primarily concerns the young and mysterious millionaire Jay Gatsby and his quixotic passion and obsession with the beautiful former debutante Daisy Buchanan. Considered to be Fitzgerald's magnum opus, The Great Gatsby explores themes of decadence, idealism, resistance to change, social upheaval, and excess, creating a portrait of the Roaring Twenties that has been described as a cautionary tale regarding the American Dream."
      isbn:
        type: "string"
        description: "The unique number that identifies a book. Can be either in the 10-character or in the 13-character format."
        example: "9781411469570"
      author_id:
        type: "integer"
        format: "int64"
        example: 1
        description: "The id of the Author"
      publisher_id:
        type: "integer"
        example: 1
        format: "int64"
        description: "The id of the Publisher"
      date_of_creation:
        type: "string"
        description: "The date that the book was added to the system"
        example: "2019-01-21"
      is_visible:
        type: "boolean"
        description: "Whether the book is visible to the public. In false, it will not be included in GET /books"
        example: true
      order_position:
        type: integer
        description: "Determines the order that the Books from the same Author will appear in GET /books"
        example: 1
  BookForCollection:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
        example: 1
      title:
        type: "string"
        example: "The Great Gatsby"
      description:
        type: "string"
        example: "The Great Gatsby is a 1925 novel written by American author F. Scott Fitzgerald that follows a cast of characters living in the fictional towns of West Egg and East Egg on prosperous Long Island in the summer of 1922. The story primarily concerns the young and mysterious millionaire Jay Gatsby and his quixotic passion and obsession with the beautiful former debutante Daisy Buchanan. Considered to be Fitzgerald's magnum opus, The Great Gatsby explores themes of decadence, idealism, resistance to change, social upheaval, and excess, creating a portrait of the Roaring Twenties that has been described as a cautionary tale regarding the American Dream."
      isbn:
        type: "string"
        description: "The unique number that identifies a book. Can be either in the 10-character or in the 13-character format."
        example: "9781411469570"
      author:
        type: object
        properties:
          id:
            type: integer
            example: 1
            description: The Author's ID
          full_name:
            type: string
            example: Scott Fitzgerald
            description: The Author's full name
  SingleBook:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
        example: 1
      title:
        type: "string"
        example: "The Great Gatsby"
      description:
        type: "string"
        example: "The Great Gatsby is a 1925 novel written by American author F. Scott Fitzgerald that follows a cast of characters living in the fictional towns of West Egg and East Egg on prosperous Long Island in the summer of 1922. The story primarily concerns the young and mysterious millionaire Jay Gatsby and his quixotic passion and obsession with the beautiful former debutante Daisy Buchanan. Considered to be Fitzgerald's magnum opus, The Great Gatsby explores themes of decadence, idealism, resistance to change, social upheaval, and excess, creating a portrait of the Roaring Twenties that has been described as a cautionary tale regarding the American Dream."
      isbn:
        type: "string"
        description: "The unique number that identifies a book. Can be either in the 10-character or in the 13-character format."
        example: "9781411469570"
      date_of_creation:
        type: "string"
        description: "The date that the book was added to the system"
        example: "2019-01-21"
      author:
        type: object
        properties:
          id:
            type: integer
            example: 1
            description: The Author's ID
          full_name:
            type: string
            example: Scott Fitzgerald
            description: The Author's full name
          email:
            type: string
            example: email@gmail.com
            description: The Author's email
          date_of_birth:
            type: string
            example: "22 of January 2019"
      publisher:
        type: object
        properties:
          id:
            type: integer
            example: 1
            description: The Author's ID
          name:
            type: string
            example: Penguin Publishing Group
            description: The Publisher's full name
          address:
            type: string
            example: "Address Str. 24"
            description: The Publisher's address
  ApiResponse:
    type: "object"
    properties:
      code:
        type: "integer"
        format: "int32"
      type:
        type: "string"
      message:
        type: "string"

externalDocs:
  description: "Find out more about Swagger"
  url: "http://swagger.io"
