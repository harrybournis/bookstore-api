# frozen_string_literal: true

module ApiHelper
  def format_errors(errors)
    { errors: errors }
  end
end
