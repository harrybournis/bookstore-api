# frozen_string_literal: true

# Publisher
class Publisher < ApplicationRecord
  has_many :books
  has_many :authors, through: :books

  validates_presence_of :name, :telephone, :address
  validates_uniqueness_of :name
end
