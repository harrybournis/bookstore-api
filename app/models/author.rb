# frozen_string_literal: true

# Author
class Author < ApplicationRecord
  has_many :books
  has_many :publishers, through: :books

  validates_presence_of :first_name, :last_name, :email
  validates_uniqueness_of :email
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }

  def full_name
    "#{first_name} #{last_name}"
  end
end
