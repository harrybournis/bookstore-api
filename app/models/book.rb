# frozen_string_literal: true

require 'isbn'

# A book
class Book < ApplicationRecord
  belongs_to :author, dependent: :destroy
  belongs_to :publisher, required: false

  before_save :set_date_of_creation

  validates_presence_of :title, :description, :author, :isbn
  validate :validate_isbn
  validate :validate_publisher_id

  def self.visible
    where(is_visible: true)
  end

  def self.with_publisher
    where.not(publisher_id: nil)
  end

  def self.ordered_alphabetically_by_author
    joins(:author).order('authors.last_name ASC')
  end

  def self.ordered_by_order_position
    order(:order_position)
  end

  private

  def validate_publisher_id
    return if publisher_id.nil? || Publisher.exists?(publisher_id)
    errors.add :publisher_id, 'must exist'
  end

  def set_date_of_creation
    self.date_of_creation = Date.today if date_of_creation.nil?
  end

  def validate_isbn
    errors.add(:isbn, 'pattern is invalid') unless isbn && ISBN.valid?(isbn)
  end
end
