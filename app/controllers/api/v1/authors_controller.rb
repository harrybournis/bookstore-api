# frozen_string_literal: true

class Api::V1::AuthorsController < ApplicationController
  def create
    author = Author.new(author_params)

    if author.save
      render json: author, serializer: Author::BaseSerializer, status: :created
    else
      render json: format_errors(author.errors), status: :bad_request
    end
  end

  private

  def author_params
    params.permit(:first_name, :last_name, :email, :date_of_birth)
  end
end
