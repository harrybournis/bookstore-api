# frozen_string_literal: true

# Bookscontroller
class Api::V1::BooksController < ApplicationController
  # GET /books
  def index
    books = Book.eager_load(:author)
                .visible
                .with_publisher
                .ordered_alphabetically_by_author
                .ordered_by_order_position

    if books.length.zero?
      render json: nil, status: :no_content
      return
    end

    render json: books, each_serializer: Book::CollectionSerializer, status: :ok
  end

  # GET /books/:id
  def show
    book = Book.eager_load(:author, :publisher)
               .where(id: params[:id])
               .first

    if book.nil?
      render json: nil, status: :no_content
      return
    end

    render json: book, serializer: Book::BaseSerializer, status: :ok
  end

  # POST /books
  def create
    book = Book.new(book_params)

    if book.save
      render json: book, serializer: Book::BaseSerializer, status: :created
    else
      render json: format_errors(book.errors), status: :bad_request
    end
  end

  # PATCH/PUT /books
  def update
    book = Book.find_by_id(params[:id])

    if book.nil?
      render json: format_errors({ id: ['does not exist'] }), status: :bad_request
      return
    end

    if book.update(book_params)
      render json: book, serializer: Book::BaseSerializer, status: :ok
    else
      render json: format_errors(book.errors), status: :bad_request
    end
  end

  # DELETE /books
  def destroy
    book = Book.find_by_id(params[:id])

    if book.nil?
      render json: format_errors({ id: ['does not exist'] }), status: :bad_request
      return
    end

    book.destroy
    render json: nil, status: :no_content
  end

  private

  def book_params
    params.permit(:title, :description, :isbn, :author_id, :publisher_id, :is_visible, :date_of_creation, :order_position)
  end

end
