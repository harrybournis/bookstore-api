class Api::V1::PublishersController < ApplicationController
  def create
    publisher = Publisher.new(publisher_params)

    if publisher.save
      render json: publisher, serializer: Publisher::BaseSerializer, status: :created
    else
      render json: format_errors(publisher.errors), status: :bad_request
    end
  end

  private

  def publisher_params
    params.permit(:name, :telephone, :address)
  end
end
