class Author::InCollectionSerializer < ActiveModel::Serializer
  attributes :id, :full_name
end
