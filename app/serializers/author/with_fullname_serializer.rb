class Author::WithFullnameSerializer < BaseSerializer
  attributes :id, :full_name, :email, :date_of_birth

  def date_of_birth
    object.date_of_birth.strftime("%d of %B %Y")
  end
end
