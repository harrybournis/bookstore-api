# frozen_string_literal: true

class Author::BaseSerializer < BaseSerializer
  attributes :id, :first_name, :last_name, :email, :date_of_birth

  def date_of_birth
    object.date_of_birth.strftime('%d of %B %Y')
  end
end
