class Book::BaseSerializer < BaseSerializer
  attributes :id, :title, :description, :isbn, :date_of_creation
  belongs_to :author, serializer: Author::WithFullnameSerializer
  belongs_to :publisher, serializer: Publisher::BaseSerializer

  def date_of_creation
    object.date_of_creation.strftime("%d-%m-%Y")
  end
end
