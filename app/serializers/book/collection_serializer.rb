class Book::CollectionSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :isbn
  belongs_to :author, serializer: Author::InCollectionSerializer

  def description
    object.description[0, 100] + '...'
  end
end
