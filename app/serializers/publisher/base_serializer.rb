class Publisher::BaseSerializer < ActiveModel::Serializer
  attributes :id, :name, :address
end
